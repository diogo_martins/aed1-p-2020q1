#include <stdio.h>
#include <math.h>

int main(int argc, char const *argv[]) {
    float a, b, c, delta_sqrt, root_1, root_2;

    scanf("%f %f %f", &a, &b, &c);
    delta_sqrt = sqrt(b * b - 4 * a * c);
    root_1 = (-b + delta_sqrt) / (2 * a);
    root_2 = (-b - delta_sqrt) / (2 * a);
    printf("%.4f %.4f\n", root_1, root_2);

    return 0;
}
