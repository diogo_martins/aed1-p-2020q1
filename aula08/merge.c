/**
 * Faz o merge out-place de duas lista ordenadas em uma lista ordenada
 */

#include <stdlib.h>
#include <stdio.h>

void merge(int* v1, int* v2, int* r, int s1, int s2) {
    int k;
    int i = 0, j = 0;

    for (k = 0; k < s1 + s2; k++) {
        // caso 1: esgotou o vetor da esquerda (consome um da direita)
        // caso 2: esgotou o vetor da direita (consome um da esquerda)
        // caso 3: o atual da esquerda eh menor que o da direita (consome o da esquerda)
        // caso 4: consumir o da direita

        if (i == s1) {
            r[k] = v2[j];
            j++;
        } else if (j == s2) {
            r[k] = v1[i];
            i++;
        } else if (v1[i] < v2[j]) {
            r[k] = v1[i];
            i++;
        } else {
            r[k] = v2[j];
            j++;
        }
    }
}

void printArray(int* v, int s) {
    int i;

    for (i = 0; i < s; i++) {
        printf("%d", v[i]);
        if (i < s - 1)
            printf(" ");
    }
    if (s > 0)
        printf("\n");
}

int main() {
    int s1, s2, i;
    int *v1, *v2, *r;

    scanf("%d", &s1);
    scanf("%d", &s2);

    v1 = malloc(s1 * sizeof(int));
    v2 = malloc(s2 * sizeof(int));
    r = malloc((s1 + s2) * sizeof(int));

    for (i = 0; i < s1; i++)
        scanf("%d", &v1[i]);

    for (i = 0; i < s2; i++)
        scanf("%d", &v2[i]);

    printArray(v1, s1);
    printArray(v2, s2);
    merge(v1, v2, r, s1, s2);
    printArray(r, s1 + s2);

    free(v1);
    free(v2);
    free(r);

    return 0;
}