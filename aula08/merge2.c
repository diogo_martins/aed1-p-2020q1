/**
 * Faz o merge in-place de um vetor semi-ordenado, ou seja, que 
 * possui duas particoes ordenadas
 */

#include <stdlib.h>
#include <stdio.h>

void merge2(int *v, int s, int m) {
    int *aux = malloc(s * sizeof(int));
    int k, i, j;

    for (k = 0; k < s; k++)
        aux[k] = v[k];

    i = 0, j = m + 1;
    for (k = 0; k < s; k++) {
        // caso 1: esgotou o subvetor da esquerda (consome um da direita)
        // caso 2: esgotou o subvetor da direita (consome um da esquerda)
        // caso 3: o atual da esquerda eh menor que o da direita (consome o da esquerda)
        // caso 4: consumir o da direita
        if (i > m) {
            v[k] = aux[j];
            j++;
        } else if (j >= s) {
            v[k] = aux[i];
            i++;
        } else if (aux[i] < aux[j]) {
            v[k] = aux[i];
            i++;
        } else {
            v[k] = aux[j];
            j++;
        }
    }

    free(aux);
}

void printArray(int* v, int s) {
    int i;

    for (i = 0; i < s; i++) {
        printf("%d", v[i]);
        if (i < s - 1)
            printf(" ");
    }
    if (s > 0)
        printf("\n");
}

int main() {
    int s, m, i;
    int *v;

    scanf("%d", &s);
    scanf("%d", &m);
    v = malloc(s * sizeof(int));

    for (i = 0; i < s; i++)
        scanf("%d", &v[i]);
    
    printArray(v, s);
    merge2(v, s, m); 
    printArray(v, s);

    free(v);

    return 0;
}