/**
 * Gera todas as biparticoes de mesmo tamanho de um array
 */

#include <stdlib.h>
#include <stdio.h>

void split(int* v, int a, int b, int indent);
void printArray(int* v, int a, int b, int indent);

/**
 * Caso base: vetor tem tamanho 1, para
 * Caso recursivo: vetor tam > 1, divide em dois, e chama um split para cada
 * particao
*/
void split(int* v, int a, int b, int indent) {
    printArray(v, a, b, indent);
    if (a == b)
        return;
    
    int m = (b - a) / 2 + a;

    indent += 2;
    split(v, a, m, indent);
    split(v, m+1, b, indent);
}

void printArray(int* v, int a, int b, int indent) {
    int i;

    for (i = 0; i < indent; i++)
        printf(" ");
    for (i = a; i <= b; i++) {
        printf("%d", v[i]);
        if (i < b)
            printf(" ");
    }
    printf("\n");
}

int main() {
    int s, i, indent = 0;
    int *v;

    scanf("%d", &s);
    v = malloc(s * sizeof(int));
    for(i = 0; i < s; i++)
        scanf("%d", &v[i]);
    
    split(v, 0, s-1, indent);
    
    free(v);

    return 0;
}
