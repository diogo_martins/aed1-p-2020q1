#include <stdlib.h>
#include <stdio.h>

void split(int* v, int* aux, int a, int b);
void merge(int *v, int* aux, int a, int m, int b);
void mergesort(int* v, int s);
void printArray(int* v, int s);

void split(int* v, int* aux, int a, int b) {
    if (b <= a)
        return;
    
    int m = (b - a) / 2 + a;

    split(v, aux, a, m);
    split(v, aux, m+1, b);
    merge(v, aux, a, m, b);
}

void merge(int *v, int* aux, int a, int m, int b) {
    int k, i, j;

    for (k = a; k <= b; k++)
        aux[k] = v[k];

    i = a, j = m + 1;
    for (k = a; k <= b; k++) {
        // caso 1: esgotou o subvetor da esquerda (consome um da direita)
        // caso 2: esgotou o subvetor da direita (consome um da esquerda)
        // caso 3: o atual da esquerda eh menor que o da direita (consome o da esquerda)
        // caso 4: consumir o da direita
        if (i > m) {
            v[k] = aux[j];
            j++;
        } else if (j > b) {
            v[k] = aux[i];
            i++;
        } else if (aux[i] < aux[j]) {
            v[k] = aux[i];
            i++;
        } else {
            v[k] = aux[j];
            j++;
        }
    }
}

void mergesort(int* v, int s) {
    int* aux = malloc(s * sizeof(int));

    split(v, aux, 0, s-1);

    free(aux);
}

void printArray(int* v, int s) {
    int i;

    for (i = 0; i < s; i++) {
        printf("%d", v[i]);
        if (i < s - 1)
            printf(" ");
    }
    if (s > 0)
        printf("\n");
}


int main() {
    int s, i;
    int* v;

    scanf("%d", &s);
    v = malloc(s * sizeof(int));

    for (i = 0; i < s; i++)
        scanf("%d", &v[i]);

    printArray(v, s);
    mergesort(v, s);
    printArray(v, s);

    free(v);

    return 0;
}