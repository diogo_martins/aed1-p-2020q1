#include <stdlib.h>
#include <stdio.h>

/**
 * A stack implemented via a dynamic linked list
 */

// ADTs
typedef struct Node Node;

struct Node {    
    int value;
    Node* next;
};

typedef Node* NodePtr;

typedef struct Stack Stack;

struct Stack {
    NodePtr top;
    int size;
};

typedef Stack* StackPtr;

// API
StackPtr newStack();
NodePtr newNode();
void stackPush(StackPtr stackPtr, int value);
int stackPop(StackPtr stackPtr);
int stackPeek(StackPtr stackPtr);
void destroyStack(StackPtr stackPtr);
int isStackEmpty(StackPtr stackPtr);
int stackSize(StackPtr stack);
void printStack(StackPtr stackPtr);

int main(int argc, char const *argv[]) {

    StackPtr stack = newStack(); 
    char op;

    while(scanf("%c", &op) != EOF) {
        if (op == 'I') {            // insert
            int n;

            scanf("%d", &n);
            stackPush(stack, n);
        } else if (op == 'R') {     // remove
            stackPop(stack);
        } else if (op == 'P') {     // print
            printStack(stack);
        } else if (op == 'S') {     // size
            printf("%d\n", stackSize(stack));
        } else if (op == 'Q') {     // peek
            printf("%d\n", stackPeek(stack));
        }
    }
    
    destroyStack(stack);

    return 0;
}

/**
 * initialize a stack adt 
 */
StackPtr newStack() {
    StackPtr stack = (StackPtr )malloc(sizeof(Stack));

    stack->top = NULL;
    stack->size = 0;

    return stack;
}

/**
 * initialize a list node
 */
NodePtr newNode(int value) {
    NodePtr node = (NodePtr )malloc(sizeof(Node));

    node->value = value;
    node->next = NULL;
}

/**
 * insert new element in the top
 */
void stackPush(StackPtr stack, int value) {
    NodePtr node = newNode(value);

    node->next = stack->top;
    stack->top = node;
    stack->size++;
}

/**
* remove the top element (and return its value)
*/
int stackPop(StackPtr stack) {
    if (isStackEmpty(stack))
        return -1;
        
    int value = stack->top->value;
    NodePtr trash = stack->top;

    stack->top = trash->next;
    stack->size--;
    free(trash);
    trash = NULL;
    
    return value;
}

/**
 * return the value of the top element
 */
int stackPeek(StackPtr stack) {
    if (isStackEmpty(stack))
        return -1;
    return stack->top->value;
}

/**
 * clear the stack memory (deallocate node and stack adt's) 
 */
void destroyStack(StackPtr stack) {
    while (!isStackEmpty(stack))
        stackPop(stack);
    free(stack);
}

/**
 * check emptiness conditions (unallocated stack or no top element) 
 */
int isStackEmpty(StackPtr stack) {
    return stack == NULL || stack->top == NULL;
}

/**
 * inform the stack size
 */
int stackSize(StackPtr stack) {
    return stack->size;
}

/**
 * print the stack as a space-separated list
 */
void printStack(StackPtr stack) {
    NodePtr cur = stack->top;

    while (cur != NULL) {
        printf("%d ", cur->value);
        cur = cur->next;
    }

    printf("\n");
}