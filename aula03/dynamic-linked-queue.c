#include <stdlib.h>
#include <stdio.h>

/**
 * A queue implemented via a dynamic linked list
 */

// ADTs
typedef struct Node Node;

struct Node {    
    int value;
    Node* next;
};

typedef Node* NodeP;

typedef struct Queue Queue;

struct Queue {
    NodeP first;
    NodeP last;
    int size;
};

typedef Queue* QueuePtr;

// API

QueuePtr newQueue();
NodeP newNode();
void enqueue(QueuePtr queue, int value);
int dequeue(QueuePtr queue);
int isQueueEmpty(QueuePtr queue);
int queueFront(QueuePtr queue);
int queueRear(QueuePtr queue);
int queueSize(QueuePtr queue);
void destroyQueue(QueuePtr queue);
void printQueue(QueuePtr queue);

/**
 * allocate a queue adt
 */
QueuePtr newQueue() {
    QueuePtr queue = (QueuePtr )malloc(sizeof(Queue));

    queue->first = NULL;
    queue->last = NULL;
    queue->size = 0;

    return queue;
}

/**
 * allocate a list node
 */
NodeP newNode(int value) {
    NodeP node = (NodeP )malloc(sizeof(Node));

    node->value = value;
    node->next = NULL;

    return node;
}

/**
 * insert new element in the front
 */
void enqueue(QueuePtr queue, int value) {
    NodeP node = newNode(value);

    if (isQueueEmpty(queue)) {        
        queue->first = node;
        queue->last = node;
    } else {
        queue->last->next = node;
        queue->last = node;
    }
    queue->size++;
}

/**
 * remove element from the rear (if any)
 */
int dequeue(QueuePtr queue) {
    if (isQueueEmpty(queue))
        return -1;
    
    NodeP trash = queue->first;
    int value = trash->value;

    queue->first = queue->first->next;
    if (queue->first == NULL)
        queue->last = NULL;
    free(trash);
    trash = NULL;
    queue->size--;

    return value;
}

/**
 * query the element at the front
 */
int queueFront(QueuePtr queue) {
    if (isQueueEmpty(queue))
        return -1;
    return queue->first->value;
}

/**
 * query the element at the rear
 */
int queueRear(QueuePtr queue) {
    if (isQueueEmpty(queue))
        return -1;
    return queue->last->value;
}

/**
 * query the queue size
 */
int queueSize(QueuePtr queue) {
    return queue->size;
}

/**
 * deallocate all node and queue adt's
 */
void destroyQueue(QueuePtr queue) {
    while (!isQueueEmpty(queue))
        dequeue(queue);
    queue->first = NULL;
    queue->last = NULL;
    free(queue);
}

/**
 * check for emptiness conditions (deallocated queue or no node)
 */
int isQueueEmpty(QueuePtr queue) {
    return queue == NULL || (queue->first == NULL && queue->last == NULL);
}

/**
 * print the queue as a space-separated list
 */
void printQueue(QueuePtr queue) {
    NodeP cur = queue->first;

    while (cur != NULL) {
        printf("%d ", cur->value);
        cur = cur->next;
    }
    printf("\n");
}

// UI
int main(int argc, char const *argv[]) {

    QueuePtr queue = newQueue(); 
    char op;

    while(scanf("%c", &op) != EOF) {
        if (op == 'I') {            // insert
            int n;

            scanf("%d", &n);
            enqueue(queue, n);
        } else if (op == 'R') {     // remove
            dequeue(queue);
        } else if (op == 'P') {     // print
            printQueue(queue);
        } else if (op == 'S') {     // size
            printf("%d\n", queueSize(queue));
        } else if (op == 'F') {     // peek first
            printf("%d\n", queueFront(queue));
        } else if (op == 'L') {     // peek last
            printf("%d\n", queueRear(queue));
        }
    }
    
    destroyQueue(queue);

    return 0;
}