#include <stdlib.h>
#include <stdio.h>

void swap(int* v, int a, int b);
void printArray(int* v, int s);
void printArray(int* v, int s);

void swap(int* v, int a, int b) {
    int aux = v[a];

    v[a] = v[b];
    v[b] = aux;
}

int partition(int* v, int s) {
    int p = s - 1;
    int i, // current position in the array
        j; // current position of the pivot

    j = 0;
    for (i = 0; i < s - 1; i++) {
        if (v[i] <= v[p]) {
            swap(v, j, i);
            j++;
        }
    }
    swap(v, p, j);

    return j;
}

void printArray(int* v, int s) {
    int i;

    for (i = 0; i < s; i++) {
        printf("%d", v[i]);
        if (i < s - 1)
            printf(" ");
    }
    printf("\n");
}

int main() {
    int s, i, p;
    int* v;

    scanf("%d", &s);
    v = malloc(s * sizeof(int));
    for (i = 0; i < s; i++) {
        scanf("%d", &v[i]);
    }
    printArray(v, s);
    p = partition(v, s);
    printf("%d\n", p);
    printArray(v, s);

    free(v);

    return 0;
}