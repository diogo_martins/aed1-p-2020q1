#include <stdlib.h>
#include <stdio.h>

void swap(int* v, int a, int b);
int partition(int* v, int a, int b);
void quicksort(int* v, int a, int b);
void sort(int* v, int s);
void printArray(int* v, int s);

void swap(int* v, int a, int b) {
    int aux = v[a];

    v[a] = v[b];
    v[b] = aux;
}

int partition(int* v, int a, int b) {
    int p = b;
    int i, // current position in the array
        j; // current position of the pivot

    j = a;
    for (i = a; i < b; i++) {
        if (v[i] <= v[p]) {
            swap(v, j, i);
            j++;
        }
    }
    swap(v, p, j);

    return j;
}

void quicksort(int* v, int a, int b) {
    if (a < b) {
        int p = partition(v, a, b);

        quicksort(v, a, p-1);
        quicksort(v, p+1, b);
    }
}

void sort(int* v, int s) {
    quicksort(v, 0, s-1);
}

void printArray(int* v, int s) {
    int i;

    for (i = 0; i < s; i++) {
        printf("%d", v[i]);
        if (i < s - 1)
            printf(" ");
    }
    printf("\n");
}

int main() {
    int s, i;
    int* v;

    scanf("%d", &s);
    v = malloc(s * sizeof(int));
    for (i = 0; i < s; i++) {
        scanf("%d", &v[i]);
    }
    printArray(v, s);
    sort(v, s);
    printArray(v, s);

    free(v);
    return 0;
}